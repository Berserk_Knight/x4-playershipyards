﻿-- param == { 0, 0, traderentity, mode }

-- modes:	- "blueprint"
--			- "licence"

-- ffi setup
local ffi = require("ffi")
local C = ffi.C
ffi.cdef[[
	typedef uint64_t UniverseID;
	typedef struct {
		const char* name;
		const char* icon;
	} LicenceInfo;
	typedef struct {
		const char* macro;
		const char* ware;
		const char* productionmethodid;
	} UIBlueprint;
	uint32_t GetNumWares(const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
	uint32_t GetBlueprints(UIBlueprint* result, uint32_t resultlen, const char* set, const char* category, const char* macroname);
	bool GetLicenceInfo(LicenceInfo* result, const char* factionid, const char* licenceid);
	uint32_t GetNumBlueprints(const char* set, const char* category, const char* macroname);
	UniverseID GetPlayerComputerID(void);
	uint32_t GetWares(const char** result, uint32_t resultlen, const char* tags, bool research, const char* licenceownerid, const char* exclusiontags);
	void LearnBlueprint(const char* wareid);
]]

-- menu variable - used by Helper and used for dynamic variables (e.g. inventory content, etc.)
local menu = {
	name = "BlueprintOrLicenceTraderMenu"
}

-- config variable - put all static setup here
--[[
local config = {
}
]]

-- init menu and register with Helper
local function init()
	Menus = Menus or { }
	table.insert(Menus, menu)
	if Helper then
		Helper.registerMenu(menu)
	end
end

-- cleanup variables in menu, no need for the menu variable to keep all the data while the menu is not active
function menu.cleanup()
	menu.playermoney = nil
	menu.totalprice = nil

	menu.blueprintstopurchase = {}
	menu.equipmenttagnames = {}
	menu.equipmenttags = {}
	menu.equipmentnumwares = {}
	menu.licencestopurchase = {}
	menu.licences = {}
	menu.primarytagnames = {}
	menu.primarytags = {}
	menu.tradecontainer = nil
	menu.traderfaction = nil
	menu.trader = nil

	menu.title = nil
	menu.header = nil
	menu.uirelations = nil

	menu.table_top = {}
	menu.table_wares = {}

	menu.expanded = {}
	menu.queueupdate = nil
	menu.selectedrow = nil
	menu.toprow = nil

	menu.frame = nil
end

function menu.onShowMenu()
	menu.trader = C.GetPlayerComputerID()
	if menu.param[3] then
		menu.trader = ConvertIDTo64Bit(menu.param[3])
	end
	menu.traderfaction = GetComponentData(menu.trader, "owner")
	menu.tradecontainer = GetContextByClass(menu.trader, "container", false)
	--print("trader: " .. tostring(GetComponentData(menu.trader, "name")) .. " container: " .. tostring(GetComponentData(menu.tradecontainer, "name")))
	menu.playermoney = GetPlayerMoney()
	menu.totalprice = 0
	menu.mode = "blueprint"
	if menu.param[4] then
		menu.mode = menu.param[4]
	end

	if menu.mode == "licence" then
		menu.title = ReadText(1001, 62)		-- Licences
		menu.header = ReadText(1001, 61)		-- Licence
		menu.uirelation = GetUIRelation(menu.traderfaction)
		menu.licences = GetOwnLicences(menu.traderfaction)
	else
		menu.title = ReadText(1001, 98)		-- "Blueprints"
		menu.header = ReadText(1001, 97)		-- "Blueprint"
		menu.primarytags = {"module", "ship", "equipment"}
		menu.primarytagnames = {ReadText(1001, 7924), ReadText(1001, 6), ReadText(1001, 7935)}	-- Modules, (Ships,) Equipment
		menu.equipmenttags = {"weapon", "turret", "shield", "engine", "thruster", "missile"}
		menu.equipmenttagnames = {ReadText(1001, 1301), ReadText(1001, 1319), ReadText(1001, 1317), ReadText(1001, 1103), ReadText(1001, 8001), ReadText(1001, 1304)}	-- Weapons, Turrets, Shields, Engines, Thrusters
		menu.equipmentnumwares = { 0, 0, 0, 0, 0, 0 }
	end

	menu.table_top = {}
	menu.expanded = {}

	menu.initData()

	-- add content
	menu.display()
end

function menu.onShowMenuSound()
	-- no sound
end

function menu.initData()
	menu.blueprintstopurchase = {}
	menu.licencestopurchase = {}

	if menu.mode == "licence" then

	else
		menu.table_wares = {}

		local numblueprints = C.GetNumBlueprints("", "", "")
		local blueprints = ffi.new("UIBlueprint[?]", numblueprints)
		numblueprints = C.GetBlueprints(blueprints, numblueprints, "", "", "")
		local blueprintlist = {}
		for i = 0, numblueprints-1 do
			blueprintlist[ffi.string(blueprints[i].ware)] = true
		end

		for _, tag in ipairs(menu.primarytags) do
			if not menu.table_wares[tag] then
				menu.table_wares[tag] = {}
			end
			if tag == "equipment" then
				for i, equipmenttag in ipairs(menu.equipmenttags) do
					if not menu.table_wares[tag][equipmenttag] then
						menu.table_wares[tag][equipmenttag] = {}
					end
					local concattag = (tag .. ", " .. equipmenttag)
					--print("tag: " .. tostring(concattag))
					local numwares = C.GetNumWares(concattag, false, menu.traderfaction, "noblueprint noplayerblueprint")
					local wares = ffi.new("const char*[?]", numwares)
					numwares = C.GetWares(wares, numwares, concattag, false, menu.traderfaction, "noblueprint noplayerblueprint")
					for j = 0, numwares-1 do
						local locware = ffi.string(wares[j])
						local locprice = (GetWareData(locware, "avgprice") * 10)
						if not blueprintlist[locware] then
							menu.table_wares[tag][equipmenttag][locware] = locprice
							menu.equipmentnumwares[i] = menu.equipmentnumwares[i] + 1
						end
					end
				end
			else
				--print("tag: " .. tostring(tag))
				local numwares = C.GetNumWares(tag, false, menu.traderfaction, "noblueprint noplayerblueprint")
				local wares = ffi.new("const char*[?]", numwares)
				numwares = C.GetWares(wares, numwares, tag, false, menu.traderfaction, "noblueprint noplayerblueprint")
				for i = 0, numwares-1 do
					local locware = ffi.string(wares[i])
					local avgprice, maxprice, hasblueprint = GetWareData(locware, "avgprice", "maxprice", "hasblueprint")
					-- DebugError("TRADERPEPE: ".. locware .. (hasblueprint and ": hasblueprint" or ": no blueprint"))
					if hasblueprint then
						local locprice = avgprice * 10
						if tag == "module" then
							locprice = maxprice
						end
						if not blueprintlist[locware] then
							menu.table_wares[tag][locware] = locprice
						end
					end
				end
			end
		end
	end
end

function menu.display()
	Helper.removeAllWidgetScripts(menu)

	local width = Helper.viewWidth / 2
	local height = Helper.viewHeight / 2
	local xoffset = width / 2
	local yoffset = height / 2

	menu.frame = Helper.createFrameHandle(menu, { width = width + 2 * Helper.borderSize, height = height + 2 * Helper.borderSize, x = xoffset - Helper.borderSize, y = yoffset - Helper.borderSize, backgroundID = "solid", backgroundColor = Helper.color.semitransparent })
	local table_bottom, row

	-- we need table_bottom first because we need the y offset of table_bottom to figure out the height of table_top
	table_bottom = menu.frame:addTable(3, { tabOrder = 2, x = Helper.borderSize, width = width })
	table_bottom:setColWidthPercent(2, 25)
	table_bottom:setColWidthPercent(3, 25)

	local row = table_bottom:addRow(false, {fixed = true, bgColor = Helper.color.transparent})
	row[1]:setBackgroundColSpan(2):createText(" ", Helper.subHeaderTextProperties)
	row[2]:setColSpan(2):createText(ReadText(1001, 2006), Helper.subHeaderTextProperties)		-- Transaction Details
	row[2].properties.halign = "center"

	row = table_bottom:addRow(false, {fixed = true, bgColor = Helper.color.transparent})
	row[2]:createText((ReadText(1001, 2005) .. ReadText(1001, 120)))		-- Transaction Value:
	row[3]:createText(function() return (ConvertMoneyString(tostring(-menu.totalprice), false, true, nil, true) .. " " .. ReadText(1001, 101)) end, {halign = "right"})
	row[3].properties.color = function() return menu.totalprice < 0 and Helper.color.green or Helper.color.red end

	local row = table_bottom:addRow(false, {fixed = true, bgColor = Helper.color.transparent})
	row[2]:createText((ReadText(1001, 2003) .. ReadText(1001, 120)))		-- Current Balance
	row[3]:createText(function() return (ConvertMoneyString(tostring(GetPlayerMoney()), false, true, nil, true) .. " " .. ReadText(1001, 101)) end, {halign = "right"})

	local row = table_bottom:addRow(false, {fixed = true, bgColor = Helper.color.transparent})
	row[2]:createText((ReadText(1001, 2004) .. ReadText(1001, 120)))		-- Final Balance
	row[3]:createText(function() return (ConvertMoneyString(tostring(GetPlayerMoney() - menu.totalprice), false, true, nil, true) .. " " .. ReadText(1001, 101)) end, {halign = "right"})

	row = table_bottom:addRow("buttonrow", {fixed = true, bgColor = Helper.color.transparent})
	row[2]:createButton({ active = function() return menu.totalprice ~= 0 end })
	row[2]:setText(ReadText(1001, 2821), {halign = "center"})		-- Confirm
	row[2].handlers.onClick = function() return menu.buttonConfirm() end
	row[3]:createButton({ active = true })
	--row[3]:createButton({ active = function() return menu.totalprice ~= 0 end })
	row[3]:setText(ReadText(1001, 64), {halign = "center"})		-- Cancel
	row[3].handlers.onClick = function() return menu.buttonCancel() end

	table_bottom.properties.y = height - table_bottom:getVisibleHeight() + Helper.borderSize

	menu.table_top = menu.frame:addTable(5, { tabOrder = 1, maxVisibleHeight = table_bottom.properties.y - Helper.borderSize, x = Helper.borderSize, y = Helper.borderSize, width = width})
	menu.table_top:setColWidth(1, Helper.scaleY(Helper.standardTextHeight), false)
	menu.table_top:setColWidth(2, Helper.scaleY(Helper.standardTextHeight), false)
	menu.table_top:setColWidthPercent(4, 25)
	menu.table_top:setColWidthPercent(5, 25)

	-- standard header
	row = menu.table_top:addRow(false, {fixed = true, bgColor = Helper.defaultTitleBackgroundColor})
	row[1]:setColSpan(5):createText(menu.title, Helper.titleTextProperties)

	row = menu.table_top:addRow(false, {fixed = true, bgColor = Helper.color.transparent})
	row[3]:createText(menu.header)
	row[4]:createText(ReadText(1001, 2808), {halign = "center"})		-- Price

	row = menu.table_top:addRow(false, {fixed = true, bgColor = Helper.color.grey})
	row[1]:setColSpan(5):createText("", {height = 1})

	if menu.mode == "licence" then
		for _, licence in ipairs(menu.licences) do
			if licence.issellable then
				local haslicence = HasLicence("player", licence.type, menu.traderfaction)
				local precursorname = ""
				local canpurchase = not haslicence
				--print("canpurchase: " .. tostring(canpurchase))
				if canpurchase then
					if licence.precursor ~= nil then
						local hasprecursor = HasLicence("player", licence.precursor, menu.traderfaction)
						local licenceinfo = ffi.new("LicenceInfo")
						C.GetLicenceInfo(licenceinfo, menu.traderfaction, licence.precursor)
						precursorname = ffi.string(licenceinfo.name)
						--print("hasprecursor: " .. tostring(hasprecursor) .. ", precursor: " .. tostring(precursorname))
						if (licence.minrelation > menu.uirelation) or (not hasprecursor) then
							canpurchase = false
						end
					elseif licence.minrelation > menu.uirelation then
						canpurchase = false
					end
				end

				row = menu.table_top:addRow(licence.id, {bgColor = Helper.color.transparent})

				row[1]:setColSpan(3):createText(licence.name, {color = function() return menu.licencestopurchase[licence.id] and Helper.color.green or Helper.standardColor end})

				row[4]:createText((ConvertMoneyString(tostring(licence.price), false, true, nil, true) .. " " .. ReadText(1001, 101)), {halign = "right", color = function() return menu.licencestopurchase[licence.id] and Helper.color.green or Helper.standardColor end})

				--print("available cash: " .. tostring(menu.playermoney - menu.totalprice) .. " price: " .. tostring(price) .. " active? " .. tostring((menu.playermoney - menu.totalprice) > price) .. " total cash: " .. tostring(menu.playermoney) .. " total price: " .. tostring(menu.totalprice))
				--print("licence: " .. tostring(licence) .. " canpurchase: " .. tostring(canpurchase))
				row[5]:createButton({ active = (haslicence and false) or (menu.licencestopurchase[licence.id] and true) or (canpurchase and (menu.playermoney - menu.totalprice) > licence.price) or false })
				row[5]:setText(function() return (HasLicence("player", licence.type, menu.traderfaction) and ReadText(1001, 84)) or (menu.licencestopurchase[licence.id] and ReadText(1001, 17)) or ReadText(1001, 3102) end, {halign = "center"})		-- Owned, Selected, Select
				row[5].handlers.onClick = function() return menu.buttonSelectLicence(licence, licence.price) end	-- TODO
				row[5].properties.bgColor = function() return menu.licencestopurchase[licence.id] and Helper.defaultArrowRowBackgroundColor or Helper.defaultButtonBackgroundColor end
				row[5].properties.mouseOverText = row[5].properties.active and "" or (canpurchase and ReadText(1026, 8101)) or (ReadText(1026, 8102))	-- "You cannot afford this licence.", "You must have better relations to purchase this licence."
				--row[5].properties.mouseOverText = row[5].properties.active and "" or (canpurchase and "You cannot afford this licence.") or (precursorname ~= "" and "You must be a " .. tostring(precursorname) .. " to purchase this licence.") or ("You must be on better terms with the " .. GetFactionData(menu.traderfaction, "name") .. " to purchase this licence.")
				AddKnownItem("licences", licence.id)
			end
		end
	else
		for tagindex, tag in ipairs(menu.primarytags) do
			row = menu.table_top:addRow(tag, {bgColor = Helper.color.transparent})
			row[1]:createButton({ active = true, height = Helper.scaleY(Helper.standardTextHeight), scaling = false })
			row[1]:setText(function() return menu.expanded[tag] and "-" or "+" end, {halign = "center"})
			row[1].handlers.onClick = function() return menu.buttonExpand(tag) end
			row[2]:setBackgroundColSpan(4)
			row[3]:createText(menu.primarytagnames[tagindex])

			if menu.expanded[tag] then
				if tag == "equipment" then
					for equipmenttagindex, equipmenttag in ipairs(menu.equipmenttags) do
						row = menu.table_top:addRow(equipmenttag, {bgColor = Helper.color.transparent})
						row[2]:createButton({ active = (menu.equipmentnumwares[equipmenttagindex] > 0), height = Helper.scaleY(Helper.standardTextHeight), scaling = false })
						row[2]:setText(function() return menu.expanded[equipmenttag] and "-" or "+" end, {halign = "center"})
						row[2].handlers.onClick = function() return menu.buttonExpand(equipmenttag) end
						row[3]:setBackgroundColSpan(3)
						row[3]:createText(menu.equipmenttagnames[equipmenttagindex], {x = Helper.standardTextOffsetx + Helper.standardIndentStep})

						if menu.expanded[equipmenttag] then
							local sortedwares = Helper.orderedKeys(menu.table_wares[tag][equipmenttag], function(a, b) return GetWareData(a, "name") < GetWareData(b, "name") end)
							for i = 1, #sortedwares do
								local ware = sortedwares[i]
								local price = menu.table_wares[tag][equipmenttag][ware]
								local licenced = HasLicence("player", GetWareData(ware, "tradelicence"), menu.traderfaction)
								local licenceinfo = ffi.new("LicenceInfo")
								local licencename = ""
								if C.GetLicenceInfo(licenceinfo, menu.traderfaction, GetWareData(ware, "tradelicence")) then
									licencename = ffi.string(licenceinfo.name)
								end
								row = menu.table_top:addRow(ware, {bgColor = Helper.color.transparent})

								row[3]:createText(GetWareData(ware, "name"), {color = function() return menu.blueprintstopurchase[ware] and Helper.color.green or Helper.standardColor end, x = Helper.standardTextOffsetx + Helper.standardIndentStep * 2})

								row[4]:createText((ConvertMoneyString(tostring(price), false, true, nil, true) .. " " .. ReadText(1001, 101)), {halign = "right", color = function() return menu.blueprintstopurchase[ware] and Helper.color.green or Helper.standardColor end})

								row[5]:createButton({ active = menu.blueprintstopurchase[ware] and true or (licenced and (menu.playermoney - menu.totalprice) > price) })
								row[5]:setText(function() return menu.blueprintstopurchase[ware] and ReadText(1001, 17) or ReadText(1001, 3102) end, {halign = "center"})		-- Selected, Select
								row[5].handlers.onClick = function() return menu.buttonSelectBlueprint(ware, price) end
								row[5].properties.bgColor = function() return menu.blueprintstopurchase[ware] and Helper.defaultArrowRowBackgroundColor or Helper.defaultButtonBackgroundColor end
								row[5].properties.mouseOverText = row[5].properties.active and "" or (licenced and ReadText(1026, 8111)) or ReadText(1026, 8112)		-- "You cannot afford this blueprint.", "You are not licenced to purchase this blueprint."
								--row[5].properties.mouseOverText = row[5].properties.active and "" or (licenced and "You cannot afford this blueprint.") or (licencename ~= "" and ("This blueprint requires the " .. licencename)) or "You are not licenced to purchase this blueprint."
							end
						end
					end
				else
					local sortedwares = Helper.orderedKeys(menu.table_wares[tag], function(a, b) return GetWareData(a, "name") < GetWareData(b, "name") end)
					for i = 1, #sortedwares do
						local ware = sortedwares[i]
						local price = menu.table_wares[tag][ware]
						local licenced = HasLicence("player", GetWareData(ware, "tradelicence"), menu.traderfaction)
						local licenceinfo = ffi.new("LicenceInfo")
						local licencename = ""
						if C.GetLicenceInfo(licenceinfo, menu.traderfaction, GetWareData(ware, "tradelicence")) then
							licencename = ffi.string(licenceinfo.name)
						end
						row = menu.table_top:addRow(ware, {bgColor = Helper.color.transparent})

						row[3]:createText(GetWareData(ware, "name"), {color = function() return menu.blueprintstopurchase[ware] and Helper.color.green or Helper.standardColor end, x = Helper.standardTextOffsetx + Helper.standardIndentStep})

						row[4]:createText((ConvertMoneyString(tostring(price), false, true, nil, true) .. " " .. ReadText(1001, 101)), {halign = "right", color = function() return menu.blueprintstopurchase[ware] and Helper.color.green or Helper.standardColor end})

						--print("available cash: " .. tostring(menu.playermoney - menu.totalprice) .. " price: " .. tostring(price) .. " active? " .. tostring((menu.playermoney - menu.totalprice) > price) .. " total cash: " .. tostring(menu.playermoney) .. " total price: " .. tostring(menu.totalprice))
						--print("ware: " .. tostring(ware) .. " trade licence: " .. tostring(GetWareData(ware, "tradelicence")) .. " player has licence: " .. tostring(HasLicence("player", GetWareData(ware, "tradelicence"), menu.traderfaction)))
						row[5]:createButton({ active = menu.blueprintstopurchase[ware] and true or (licenced and (menu.playermoney - menu.totalprice) > price) })
						row[5]:setText(function() return menu.blueprintstopurchase[ware] and ReadText(1001, 17) or ReadText(1001, 3102) end, {halign = "center"})		-- Selected, Select
						row[5].handlers.onClick = function() return menu.buttonSelectBlueprint(ware, price) end
						row[5].properties.bgColor = function() return menu.blueprintstopurchase[ware] and Helper.defaultArrowRowBackgroundColor or Helper.defaultButtonBackgroundColor end
						row[5].properties.mouseOverText = row[5].properties.active and "" or (licenced and ReadText(1026, 8111)) or ReadText(1026, 8112)		-- "You cannot afford this blueprint.", "You are not licenced to purchase this blueprint."
						--row[5].properties.mouseOverText = row[5].properties.active and "" or (licenced and "You cannot afford this blueprint.") or (licencename ~= "" and ("This blueprint requires the " .. licencename)) or "You are not licenced to purchase this blueprint."
					end
				end
			end
		end
	end

	if menu.selectedrow then
		menu.table_top:setSelectedRow(menu.selectedrow)
		menu.selectedrow = nil
		if menu.toprow then
			menu.table_top:setTopRow(menu.toprow)
			menu.toprow = nil
		end
	end

	menu.frame:display()
end

-- widget scripts
function menu.buttonExpand(tag)
	if menu.expanded[tag] then
		menu.expanded[tag] = nil
	else
		menu.expanded[tag] = true
	end

	menu.toprow = GetTopRow(menu.table_top.id)
	menu.selectedrow = Helper.currentTableRow[menu.table_top.id]
	menu.queueupdate = true
end

function menu.buttonConfirm()
	if menu.mode == "licence" then
		for licenceid, licenceinfo in pairs(menu.licencestopurchase) do
			--print("adding licence " .. tostring(licenceid) .. " from " .. tostring(menu.traderfaction))
			AddLicence("player",  licenceinfo.type, menu.traderfaction)
		end
	else
		for ware, _ in pairs(menu.blueprintstopurchase) do
			-- DebugError("LEARNING BLUEPRINT: ".. tostring(ware))
			C.LearnBlueprint(ware)
			-- DebugError("LEARNING BLUEPRINT: ".. tostring(ware) .." WE DIDN'T CRASH")
		end
	end
	TransferPlayerMoneyTo(menu.totalprice, menu.tradecontainer)
	menu.initData()
	menu.tallyTotalPrice()
	-- activate this and remove Helper.closeMenuAndReturn() and menu.cleanup if we later decide to keep the menu open.
	--[[
	-- NB: doing this here rather than in onUpdate so that hitting Cancel resets your position in the menu. remove here and activate in onUpdate if we prefer that the menu position is never reset.
	menu.toprow = GetTopRow(menu.table_top.id)
	menu.selectedrow = Helper.currentTableRow[menu.table_top.id]
	-- NB: we only need to refresh the menu here because we cannot set button.properties.active to a function and run those functions upon frame:update()
	menu.queueupdate = true
	]]
	Helper.closeMenuAndReturn(menu)
	menu.cleanup()
end

function menu.buttonCancel()
	menu.blueprintstopurchase = {}
	menu.licencestopurchase = {}
	menu.tallyTotalPrice()
	-- activate this and remove Helper.closeMenuAndReturn() and menu.cleanup if we later decide to keep the menu open.
	--[[
	menu.expanded = {}
	-- NB: we only need to refresh the menu here because we cannot set button.properties.active to a function and run those functions upon frame:update()
	menu.queueupdate = true
	]]
	Helper.closeMenuAndReturn(menu)
	menu.cleanup()
end

function menu.buttonSelectLicence(licence, price)
	if not menu.licencestopurchase[licence.id] then
		menu.licencestopurchase[licence.id] = { type = licence.type, price = price }
	else
		menu.licencestopurchase[licence.id] = nil
	end
	-- NB: doing this here rather than in onUpdate so that hitting Cancel resets your position in the menu. remove here and activate in onUpdate if we prefer that the menu position is never reset.
	menu.toprow = GetTopRow(menu.table_top.id)
	menu.selectedrow = Helper.currentTableRow[menu.table_top.id]
	menu.tallyTotalPrice()
	menu.queueupdate = true
end

function menu.buttonSelectBlueprint(ware, price)
	if not menu.blueprintstopurchase[ware] then
		menu.blueprintstopurchase[ware] = price
	else
		menu.blueprintstopurchase[ware] = nil
	end
	-- NB: doing this here rather than in onUpdate so that hitting Cancel resets your position in the menu. remove here and activate in onUpdate if we prefer that the menu position is never reset.
	menu.toprow = GetTopRow(menu.table_top.id)
	menu.selectedrow = Helper.currentTableRow[menu.table_top.id]
	menu.tallyTotalPrice()
	menu.queueupdate = true
end

-- helper functions
function menu.tallyTotalPrice()
	menu.totalprice = 0
	if menu.mode == "licence" then
		for licenceid, licenceinfo in pairs(menu.licencestopurchase) do
			menu.totalprice = menu.totalprice + licenceinfo.price
		end
	else
		for ware, price in pairs(menu.blueprintstopurchase) do
			menu.totalprice = menu.totalprice + price
		end
	end
end

menu.updateInterval = 0.1

function menu.onUpdate()
	if menu.queueupdate then
		menu.queueupdate = nil
		menu.display()
	end
end

function menu.onCloseElement(dueToClose)
	Helper.closeMenuAndReturn(menu)
	menu.cleanup()
end

init()
